from setuptools import setup, find_packages

setup(
    name="omnigen_auth",
    version="1.1",
    url="https://bitbucket.org/omnigen/py-omnigen-auth",
    author="Wesley Ameling",
    author_email="w.ameling@omnigen.nl",
    license="MIT",
    packages=find_packages(),
    install_requires=["requests"],
    zip_safe=False,
)
