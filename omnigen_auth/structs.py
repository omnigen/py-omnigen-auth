import re


class InvalidResponseError(Exception):
    """
    Exception when the response of the auth server was unexpected
    """

    def __init__(self, response):
        super().__init__(f'Unexpected response: {response}')


class MissingKwargError(Exception):
    """
    Exception when a kwarg could not be found
    """

    def __init__(self, item):
        super().__init__(f'The kwargs {item} could not be found')


class InvalidCredentialsError(Exception):
    """
    Exception when no credentials whatsover are provided
    """

    def __init__(self):
        super().__init__(
            'No credentials provided, not running as app engine project')


class _BaseUser(object):
    """
    An object for shared data between every type of grant type (authorization
    code, client credentials)
    """
    _type = ''

    def __init__(self, valid, data):
        # Check the type
        _type = data.get('type', None)
        # Set the base variables
        self._user_id = None
        self._client_id = None
        self._valid = valid
        # If valid, retrieve data from the object
        if valid:
            # Check the type
            if self._type != _type:
                del data['type']
                raise ValueError(
                    f'Invalid type {self._type} for {self.__class__.__name__}')
            self._user_id = data.get('uid', None)
            self._client_id = data.pop('client_id')
            self._scopes = tuple(re.split(r'\s+', data.pop('scopes')))

    @property
    def uid(self):
        """
        UID of the user associated with the token
        :return: The UID of the token
        :rtype: str
        """
        return self._user_id

    @property
    def client_id(self):
        """
        The client ID of the associated token, always available
        :return: The client ID of the associated token
        :rtype: str
        """
        return self._client_id

    @property
    def scopes(self):
        """
        Scopes associated with the token. Currently not really used..
        :return: The scopes of the associated token
        :rtype: list
        """
        return self._scopes

    @property
    def valid(self):
        """
        Whether the associated access token is even valid
        :return: Whether the associated access token is even valid
        :rtype: bool
        """
        return self._valid

    def is_user_logged_in(self):
        """
        Whether this object represents an user
        :return: Whether this object represents an user
        :rtype: bool
        """
        return self._valid and self._user_id is not None

    def is_client_logged_in(self):
        """
        Whether this object represents *just* a client
        :return: Whether this objects *just* a client
        :rtype: bool
        """
        is_client = self._valid and self._client_id is not None
        return is_client and not self.is_user_logged_in()


class User(_BaseUser):
    """
    An object which represent an user associated with the token
    """
    _type = 'user'

    def __init__(self, valid, data):
        super().__init__(valid, data)
        if valid:
            self._email = data.pop('email')
            self._locked = data.pop('locked')
            self._username = data.get('username', None)
            self._first_name = data.get('first_name', None)
            self._last_name = data.get('last_name', None)
            self._locale = data.get('locale', None)

    @property
    def email(self):
        """
        The e-mail address of the user
        :return: The e-mail address of the user
        :rtype: str
        """
        return self._email

    @property
    def locked(self):
        """
        Whether this user is locked. This always is true when this object
        is retrieved from an access token.
        :return: Whether the user is locked
        :rtype: bool
        """
        return self._locked

    @property
    def first_name(self):
        """
        The first name of the user when supplied in the data. This returns an
        empty string when it was supplied and None when not available for the
        sake of backwards compatibility.
        :return: A string representing the first name of the user
        :rtype: str
        """
        return self._first_name

    @property
    def last_name(self):
        """
        The last name of the user when supplied in the data. This returns an
        empty string when it was supplied and None when not available for the
        sake of backwards compatibility.
        :return: A string representing the last name of the user
        :rtype: str
        """
        return self._last_name

    @property
    def username(self):
        """
        The username of the use when supplied in the data. This likely may
        return an empty string or None when not available.
        :return: The username of the user
        :rtype: str
        """
        return self._username

    @property
    def locale(self):
        """
        The locale associated with the user. When this is not available on
        the authorization server (because of an older version), this function
        will return None.
        :return: A string representing the locale of the user
        :rtype: str
        """
        return self._locale


class Client(_BaseUser):
    """
    An object which represents *just* a client, not an User. Note that this
    does not have additional properties.
    """
    _type = 'client'


def response_factory(data):
    """
    Transforms the response of the auth server to an object

    :param data: The data to transform
    :type data: dict
    :return: Either Client or User
    :rtype: _BaseUser
    """
    if not data['valid']:
        return User(data['valid'], data)

    _type = data['data'].get('type')
    if _type != 'user' and _type != 'client':
        raise ValueError(f'Unexpected type: {_type}')

    _object = User
    if _type == 'client':
        return Client
    return _object(data.pop('valid'), data.pop('data'))
