from .structs import Client, User


class MockUser(User):
    """ User object for testing purposes """

    def __init__(self, uid, client_id=None):
        data = dict(
            uid=uid,
            client_id=client_id or "random_client_id",
            email="test@omnigen.nl",
            scopes="",
            locked=False,
        )
        data["type"] = "user"
        super().__init__(True, data)


class MockClient(Client):
    """ Client object for testing purposes """

    def __init__(self, client_id=None):
        data = dict(client_id=client_id or "random_client_id", scopes="")
        data["type"] = "client"
        super().__init__(True, data)
