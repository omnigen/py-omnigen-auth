import functools
from base64 import b64encode

import requests

from .structs import *


def _get_kwarg(item, source):
    """
    Retrieves a key from the source, but when missing will raise a missing
    kwarg exception

    :param item: The item to retrieve
    :type item: str
    :param source: The source to retrieve the item from
    :type source: dict

    :return: The associated value in the source from the key
    :rtype: object
    """
    if item not in source:
        raise MissingKwargError(item)
    return source.pop(item)


def _get_invalid_response(response, return_dummy=True):
    if return_dummy:
        return User(False, dict())
    raise InvalidResponseError(response)


def also_unbound(func):
    """
    A decorator which allows methods of the AccessTokenValidator also to be
    unbound as long as credentials are provided

    :param func: The function to wrap
    :return: The wrapper method
    """

    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        args = list(args)
        if len(args) == 1:
            # the method is unbound!
            auth_base_url = _get_kwarg('auth_base_url', kwargs)
            consumer_id = _get_kwarg('consumer_id', kwargs)
            consumer_secret = _get_kwarg('consumer_secret', kwargs)
            validator = AccessTokenValidator(
                auth_base_url, consumer_id, consumer_secret)
        else:
            validator = args[0]
            del args[0]
        return func(validator, *args, **kwargs)

    return wrapper


class AuthApi(object):
    """
    Object to make authenticated requests with
    """

    def __init__(self, auth_base_url, consumer_id=None, consumer_secret=None):
        self._base_endpoint = auth_base_url
        if self._base_endpoint[-1] != '/':
            self._base_endpoint += '/'
        if not consumer_id or not consumer_secret:
            raise InvalidCredentialsError()
        self._base_auth = None
        if consumer_id and consumer_secret:
            self._base_auth = 'Basic ' + b64encode(
                '{}:{}'.format(consumer_id, consumer_secret)
                    .encode()).decode()

    def _make_request(self, endpoint, method='GET', headers=None, **kwargs):
        if headers is None:
            headers = dict()
        if self._base_auth:
            headers['Authorization'] = self._base_auth

        if endpoint[0] == '/':
            endpoint = endpoint[1:]
        url = self._base_endpoint + endpoint
        return requests.request(method.lower(), url, headers=headers, **kwargs)

    def validate_access_token(self, access_token):
        return self._make_request(
            'api/validate', params=dict(access_token=access_token))

    def revoke_access_token(self, access_token):
        return self._make_request(
            'api/revoke', params=dict(access_token=access_token))

    def invite_user(self, client_id, email, redirect_uri):
        return self._make_request(
            'api/invite-user', params=dict(client_id=client_id, email=email, redirect_uri=redirect_uri),
            method='POST')

    def list_users(self, client_id):
        return self._make_request(
            'api/list-users', params=dict(client_id=client_id), method='GET')

    def get_user(self, uid):
        return self._make_request(
            'api/get-user', params=dict(user_uid=uid), method='GET')

    def get_user_object(self, uid, return_dummy=False):
        response = self.get_user(uid)
        if not response.ok:
            return _get_invalid_response(response, return_dummy)
        try:
            parsed_data = response.json()
        except ValueError:
            return _get_invalid_response(response, return_dummy)
        parsed_data['valid'] = True
        return response_factory(parsed_data)

    def remove_user(self, uid, send_gdpr_mail=True):
        send_mail = int(bool(send_gdpr_mail))
        return self._make_request(
            'api/remove-user', params=dict(user_uid=uid, send_mail=send_mail),
            method='DELETE')

    def lock_user(self, uid):
        return self._make_request(
            'api/lock-user', params=dict(user_uid=uid), method='POST')

    def unlock_user(self, uid):
        return self._make_request(
            'api/unlock-user', params=dict(user_uid=uid), method='POST')

    def list_signups(self, client_id):
        return self._make_request(
            'api/list-signups', params=dict(client_id=client_id), method='GET')

    def revoke_signup(self, signup_id):
        return self._make_request(
            'api/revoke-signup', params=dict(signup_id=signup_id),
            method='POST')

    def search_users(self, query, offset=-1, max_items=-1, fields=None,
                     client_id=None):
        parameters = dict(query=query)
        if offset >= 0:
            parameters['offset'] = offset
        if max_items >= 0:
            parameters['max_items'] = max_items
        if fields:
            parameters['fields'] = ','.join(fields)
        if client_id:
            parameters['client_id'] = client_id
        return self._make_request(
            'api/search', params=parameters, method='GET')


class AccessTokenValidator(object):
    """
    The object which will make a request to the authorization server and
    convert it to a response _BaseUser object
    """

    def __init__(self, auth_base_url, consumer_id, consumer_secret,
                 return_dummy=True):
        self._api = AuthApi(
            auth_base_url, consumer_id=consumer_id,
            consumer_secret=consumer_secret)
        self._return_dummy = return_dummy

    def set_return_dummy(self, return_dummy):
        """
        When an invalid request occurs, this determines whether an
        InvalidResponseError gets raised or dummy user object gets returned
        which is not authenticated.

        :param return_dummy: Whether to return a dummy object or not
        :type return_dummy: bool
        """
        self._return_dummy = bool(return_dummy)

    @also_unbound
    def get_access_token_data(self, access_token):
        """
        Makes the request to the authorization server to check whether the
        access token is valid or not.

        :param access_token: The access token to check
        :type access_token: str
        :return: The _BaseUser object associated with the request
        :rtype: _BaseUser

        :raises InvalidResponseError: When the response of the auth server was \
            unexpected
        """
        response = self._api.validate_access_token(access_token)
        if not response.ok:
            return _get_invalid_response(response, self._return_dummy)
        try:
            parsed_data = response.json()
        except ValueError:
            return _get_invalid_response(response, self._return_dummy)
        return response_factory(parsed_data)

    @also_unbound
    def is_access_token_valid(self, access_token):
        """
        Only use this method if you only care if you have a valid access token.
        Note that you are not able check whether the access token is bound to
        either an user or client this way, any will do!

        :param access_token: The access token to check
        :type access_token: str

        :return: Whether the access token is valid or not
        :rtype: bool

        :raises InvalidResponseError: When the response of the auth server was \
            unexpected
        """
        return self.get_access_token_data(access_token).valid
